package com.example.android.favoritetoys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TextView mToysListTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToysListTextView = (TextView) findViewById(R.id.tv_toy_names);

        String [] toyNames = {"Duck", "Batman", "Superman", "Iron Man", "GTX 1060", "MSI GE63", "Cooler Boost", "Mystic Light", "SteelSeries", "3D Touch", "PHP", "Javascript", "HTML", "Node", "React Native", "Lorem", "Ipsum Bieum", "Out of Words"};

        for(String toyName : toyNames){
            mToysListTextView.append(toyName + "\n\n\n");
        }
    }
}
